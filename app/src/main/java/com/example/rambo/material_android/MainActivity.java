package com.example.rambo.material_android;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String COMMON_TAG = "CombinedLifeCycle";
    private static final String ACTIVITY_NAME = MainActivity.class.getSimpleName();
    private static final String TAG = ACTIVITY_NAME;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    private Button buttonAddFragment;
    private Button showCount;
    private TextView textViewFragmentCountb;
    private TextView textViewFragmentCountf;
    int fragCount=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonAddFragment = (Button)findViewById(R.id.buttonAddFragment);
        showCount = (Button)findViewById(R.id.showCount);
        textViewFragmentCountb = (TextView)findViewById(R.id.textViewFragmentCountb);
        textViewFragmentCountf = (TextView)findViewById(R.id.textViewFragmentCountf);
        fragmentManager=getSupportFragmentManager();

        /*
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                 textViewFragmentCountb.setText("Fragment count in back stack: "+fragmentManager.getBackStackEntryCount());
            }
        });
*/
        //Log.i(TAG,"Initial BackStackEntryCount: "+fragmentManager.getBackStackEntryCount());


        buttonAddFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addFragment();
            }
        });

        showCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number_of_frag = Integer.toString(fragmentManager.getFragments().size()); ;
                String number_of_frag_back =  Integer.toString(fragmentManager.getBackStackEntryCount());

                textViewFragmentCountb.setText("Fragment count in back stack: "+number_of_frag_back);
                textViewFragmentCountf.setText("Fragment count  in activity: "+number_of_frag);
            }
        });




    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void addFragment(){


        Fragment fragment;
        switch (fragCount){
            case 0: fragment = new SampleFragment(); break;
            case 1: fragment = new FragmentTwo();break;
            case 2: fragment = new FragmentThree(); break;
            default: fragment = new SampleFragment(); break;
        }

        fragCount++;


        fragmentTransaction=fragmentManager.beginTransaction();
      //  fragmentTransaction.add(R.id.fragmentContainer,fragment,"demofragment");



        fragmentTransaction.replace(R.id.fragmentContainer,fragment, "fragment  " );
      //  fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();


    }
    @Override
    public void onBackPressed(){
       Fragment fragment = fragmentManager.findFragmentById(R.id.fragmentContainer);

       if(fragment!=null)
       {
           fragmentTransaction=fragmentManager.beginTransaction();
           fragmentTransaction.remove(fragment );
           //  fragmentTransaction.addToBackStack(null);
           fragmentTransaction.commit();
       }
       else
       {
           super.onBackPressed();
       }
    }
}
